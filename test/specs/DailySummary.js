var fs = require('fs');
var firebase = require("firebase");
var config = {
    apiKey: "AIzaSyDnlfx27odAJFxOxpAoJPdf58REoGtjN5c",
    authDomain: "jse-data-e8576.firebaseapp.com",
    databaseURL: "https://jse-data-e8576.firebaseio.com/",
    storageBucket: "bucket.appspot.com"
};

firebase.initializeApp(config);


// Get a reference to the database service
var database = firebase.database();


describe('Get Market Summary', () => {
    before('Go To Page', async () => {
        await browser.url('https://www.jamstockex.com/')
    })

    it('Get Live Data', async () => {
        let amount = await browser.$$('#ticker-container > div > div > div > div > div > div > ul > li > a')
        let array = []
        for (let i = 3; i < amount.length; i++) {

            let id = i + 1;
            let selector = await browser.$(`#ticker-container > div > div > div > div > div > div > ul > li:nth-child(${i}) > a`)
            let html = await selector.getHTML()
            let name = await selector.getAttribute('href')
            if (name.includes('symbol') === true) {
                name = name.split('=')[1].toLocaleUpperCase()
            } else {
                name = name.split('index-data/')[1].toLocaleUpperCase()
            }

            let volume = html.split('Vol')[0].split('<br>')[1].replace(/(\r\n|\n|\r)/gm, "").replace(/ /g, '')
            let closePrice = html.split('Vol')[1].split('>')[1].split('<')[0].replace(/(\r\n|\n|\r)/gm, "").trim().replace(/ /g, '')
            let status = html.split('img/')[1].split('.png')[0]
            let priceChange = html.split('.png">')[1].split('</a>')[0].replace(/(\r\n|\n|\r)/gm, "").replace(/ /g, '')

            let data = {
                id,
                name,
                volume,
                closePrice,
                status,
                priceChange
            }
            array[i] = data


        }

        var filtered = array.filter(function (el) {
            return el != null;
        });
        database.app.database().ref('LiveMarketData').set(
            filtered
        )
        console.log('Market Updated')
    })

})


